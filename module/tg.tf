#creating target groups
resource "aws_lb_target_group" "web_tg" {
  name     = "web"
  port     = 80
  protocol = "HTTP"
  vpc_id   = var.VPC_ID
}

#creating target group attachment

resource "aws_lb_target_group_attachment" "web_tg_attachment" {
  count            =  1
  target_group_arn = aws_lb_target_group.web_tg.arn
  target_id        = element(var.INSTANCE_ID,count.index )
  port             = 80
}
#Adding listenre

resource "aws_lb_listener" "front_end" {
  load_balancer_arn = aws_lb.robo_shop_alb.arn
  port              = "80"
  protocol          = "HTTP"
//  ssl_policy        = "ELBSecurityPolicy-2016-08"
//  certificate_arn   = "arn:aws:iam::187416307283:server-certificate/test_cert_rab3wuqwgja25ct3n4jdj2tzu4"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.web_tg.arn
  }
}