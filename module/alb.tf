resource "aws_lb" "robo_shop_alb" {
  name               = "test-lb-tf"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.allow_web_insecure.id]
  subnets            = var.PUBLIC_SUBNTS_IDS


  tags = {
    Environment = "DEV"
  }
}