provider "aws" {
  region = "us-east-1"
}
terraform {
  backend "s3" {
    bucket = "sudheer919"
    key    = "project/alc/dev/terraform.tfstate"
    region = "us-east-1"
    //  dynamodb_table = "tlocking9"
  }
}
#import vpc tfstate

data "terraform_remote_state" "vpc" {
  backend = "s3"
  config = {
    bucket = "sudheer919"
    key    = "project/vpc/dev/terraform.tfstate"
    region = "us-east-1"
  }
}

#import ec2 tfstate

data "terraform_remote_state" "ec2" {
  backend = "s3"
  config = {
    bucket = "sudheer919"
    key    = "project/ec2/dev/terraform.tfstate"
    region = "us-east-1"
  }
}