module "alb" {
  source                  = "../module"
  MANAGEMENT_VPC_ID       = data.terraform_remote_state.vpc.outputs.MANAGEMENT_VPC_ID
  PRIVATE_SUBNETS_IDS     = data.terraform_remote_state.vpc.outputs.PRIVATE_SUBNETS_IDS
  PUBLIC_SUBNTS_IDS       = data.terraform_remote_state.vpc.outputs.PUBLIC_SUBNTS_IDS
  VPC_ID                  = data.terraform_remote_state.vpc.outputs.VPC_ID
  INSTANCE_ID             = data.terraform_remote_state.ec2.outputs.INSTANCE_ID
}